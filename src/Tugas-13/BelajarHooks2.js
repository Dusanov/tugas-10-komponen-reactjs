import React, {useState, useEffect} from "react"
import axios from 'axios';

const BuahHooks = () => {

	const [dataHargaBuah, set_dataHargaBuah] = useState(null)
	const [inputName, setName] = useState("")
	const [inputPrice, setPrice] = useState("")
	const [inputWeight, setWeight] = useState("")
	const [flagInputEdit, set_flagInputEdit] = useState(null)

	const handle_nameChange = (event) => {
		var nilai = event.target.value;
		setName(nilai);
		//console.log(nilai)
	}

	const handle_priceChange = (event) => {
		var nilai = event.target.value;
		setPrice(nilai);
		//console.log(nilai)
	}

	const handle_weightChange = (event) => {
		var nilai = event.target.value;
		setWeight(nilai);
		//console.log(nilai)
	}

	const handle_submitBuah = (event) => {
		// menahan submit
		event.preventDefault()

		var objek_yangMauDiPost = {
			name: inputName,
			price: inputPrice,
			weight: inputWeight
		}

		if (flagInputEdit == null) {

			axios.post(`http://backendexample.sanbercloud.com/api/fruits`, objek_yangMauDiPost)
				.then(res => {
					//console.log(res)
					var data = res.data;
					set_dataHargaBuah([...dataHargaBuah, data])
					
					setName("");
					setPrice("");
					setWeight("");
				}
			)

		} else {

			var objek_yangMauDiPut = dataHargaBuah.find(x=> x.id == flagInputEdit);

			objek_yangMauDiPut.name = inputName;
			objek_yangMauDiPut.price = inputPrice;
			objek_yangMauDiPut.weight = inputWeight;

			axios.put(`http://backendexample.sanbercloud.com/api/fruits/${flagInputEdit}`, objek_yangMauDiPut)
				.then(res => {

					var data = res.data;
					var new_dataHargaBuah = dataHargaBuah.map(x=> {

						if(x.id == flagInputEdit){
							x.name = inputName;
							x.price = inputPrice;
							x.weight = inputWeight;
						}
						return x;
					})

					set_dataHargaBuah(new_dataHargaBuah);
					setName("");
					setPrice("");
					setWeight("");
				}
			)
		}
	}

	const handle_hapusBuah = (event) => {

		var idBuah_yangMauDiDelete = parseInt(event.target.value);

		axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah_yangMauDiDelete}`)
			.then(res => {
			//console.log(res);
			var new_dataHargaBuah = dataHargaBuah.filter(x=> x.id !== idBuah_yangMauDiDelete);
			set_dataHargaBuah(new_dataHargaBuah);
		})

	}

	const handle_editBuah = (event) => {

		//ambil id
		var idBuah_yangMauDiEdit = event.target.value;

		//ambil objek
		var buah_yangMauDiEdit = dataHargaBuah.find(x=> x.id == idBuah_yangMauDiEdit);

		//ubah value dalam input Nama, Price, Weight
		setName(buah_yangMauDiEdit.name);
		setPrice(buah_yangMauDiEdit.price);
		setWeight(buah_yangMauDiEdit.weight);

		set_flagInputEdit(idBuah_yangMauDiEdit);
	}

	useEffect( () => {
		if(dataHargaBuah == null){
			axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
				.then(res => {
					//console.log(res.data)
					set_dataHargaBuah(res.data)
			})
		}
	})

  	return (
		<>

			<h1>Tabel Harga Buah</h1>

			<table style={{border: "1px solid #000", padding: "2px"}}>
				<thead>
					<tr style={{backgroundColor: "grey"}}>
						<th style={{border: "1px solid #000"}}>Index</th>
						<th style={{border: "1px solid #000"}}>Nama</th>
						<th style={{border: "1px solid #000"}}>Harga</th>
						<th style={{border: "1px solid #000"}}>Berat</th>
					</tr>
				</thead>

				<tbody>
				{
					dataHargaBuah !== null && dataHargaBuah.map((item, index)=>{
					return(                    
						<tr key={item.id}>
							<td>{index+1}</td>
							<td>{item.name}</td>
							<td>{item.price}</td>
							<td>{item.weight}</td>
							<td>
								<button value={item.id} onClick={handle_editBuah}>Edit</button>
								&nbsp;
								<button value={item.id} onClick={handle_hapusBuah}>Delete</button>
							</td>
						</tr>
					)
				})
				}
				</tbody>



			</table>

			<form onSubmit={handle_submitBuah}>
				<label>Name:</label>
				<input type="text" value={inputName} onChange={handle_nameChange} /> <br/>

				<label>Price:</label>
				<input type="text" value={inputPrice} onChange={handle_priceChange} /> <br/>

				<label>Weight:</label>
				<input type="text" value={inputWeight} onChange={handle_weightChange}/> <br/>

				<button>submit</button>
	        </form>

		</>
    )
}


export default BuahHooks;