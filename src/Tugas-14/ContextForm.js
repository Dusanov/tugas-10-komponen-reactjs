import React, {useContext, useState} from "react"
import {ContextBuah} from "./ContextBuah"

const ContextForm = () =>{

  const [fruit, setFruit, inputForm, setInputForm] = useContext(ContextBuah)
  //const [name, setName] = useState(inputForm.name)
  //const [price, setPrice] = useState(inputForm.price)
  //const [weight, setWeight] = useState(inputForm.weight)
  
  const handleSubmit = (event) =>{
    
    event.preventDefault()
    var newId = fruit.length +1
    if (inputForm.id === null) {
      setFruit([...fruit, {name: inputForm.name, price:inputForm.price, weight:inputForm.weight, id: newId}])
      }else{
        var singleFruit = fruit.find(x=> x.id === inputForm.id)
        singleFruit.name = inputForm.name
        singleFruit.price = inputForm.price
        singleFruit.weight = inputForm.weight
        setFruit([...fruit])
      }
    
    setInputForm({name: "", price: 0, weight:0, id: null})
    //setName("")
    //setPrice("")
    //setWeight("")
  
  }
  
  const handleChangeName = (event) =>{
    setInputForm({...inputForm, name: event.target.value})
  
  }

  const handleChangePrice = (event) =>{
    setInputForm({...inputForm, price: event.target.value})
    //setPrice(event.target.value)
  }

  const handleChangeWeight = (event) =>{
    setInputForm({...inputForm, weight: event.target.value})
    //setWeight(event.target.value)
  }

  return(
    <>
      <form onSubmit={handleSubmit}>
        
        <strong>Name Fruit</strong><input type="text" value={inputForm.name} onChange={handleChangeName} /><br/>
        <strong>Price</strong><input type="number" value={inputForm.price} onChange={handleChangePrice} /><br/>
        <strong>Weight</strong><input type="number" value={inputForm.weight} onChange={handleChangeWeight} />
        <br/>
        <button>submit</button>
      
      </form>
    </>
  )

}

export default ContextForm