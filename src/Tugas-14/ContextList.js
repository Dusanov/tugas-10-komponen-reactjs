import React, {useContext} from "react"
import {ContextBuah} from "./ContextBuah"

const ContextList = () => {
  const [fruit, setFruit, inputForm, setInputForm] = useContext(ContextBuah)

  const handleDelete= (event) => {
  	var idFruit = parseInt(event.target.value)
  		var newFruit = fruit.filter(x=> x.id !== idFruit)
  		setFruit([...newFruit])

  }

  const handleEdit= (event) => {
  		var idFruit = parseInt(event.target.value)
  		var singleFruit = fruit.find(x=> x.id === idFruit)
  		setInputForm({...inputForm, name: singleFruit.name, price: singleFruit.price, weight: singleFruit.weight, id: idFruit})
  }

  return (
  		<table>
  		 <thead>
  			<tr>
  				<th>Name</th>
  				<th>Price</th>
  				<th>Weight</th>
  				<th>Action</th>
  			</tr>
  		 </thead>
  		 <tbody>
	    
	      {fruit.map(el=>{
	      	return (
	      		<tr>
	      			<td>{el.name}</td>
	      			<td>{el.price}</td>
	      			<td>{el.weight}</td>
	      			<td>
	      				<button value={el.id} style={{marginRight:"10px"}}onClick={handleEdit}>Edit</button>
	      				<button value={el.id} onClick={handleDelete}>Delete</button>
	      			</td>
	      		</tr>
	      	)
	        	
	      })}

	     </tbody>
	    </table>
	)
}

export default ContextList