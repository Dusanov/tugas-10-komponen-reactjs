import React,  {Component} from 'react';


class Daftar extends Component {

	constructor(props){
	    super(props)
	    this.state ={
	        dataHargaBuah : [
			  { name: "Semangka", price: 10000, weight: 1000} ,
			  { name: "Anggur", price: 40000, weight: 500 },
			  { name: "Strawberry", price: 30000, weight: 400 },
			  { name: "Jeruk", price: 30000, weight: 1000 },
			  { name: "Mangga", price: 30000, weight: 500 }
			],
			inputName : "",
			indexBuah : -1
	    }

	}

  	ubahInput = (event) => {
		var value = event.target.value;
    	this.setState({inputName: value});
  	}

	editForm = (event) => {
		var index = event.target.value;
		var objek_nilaiBarisTabel = this.state.dataHargaBuah[index];
	
		this.setState({
			inputName : objek_nilaiBarisTabel.name,
			indexBuah : index
		})
	}

	handleSubmit = (event) => {
		var index = this.state.indexBuah;

    	event.preventDefault()

    	if (this.state.indexBuah == -1) {
	    	var objek_yangMauDitambah = {name: this.state.inputName, price: 0, weight: 0}

	    	this.setState({
	    		dataHargaBuah: [...this.state.dataHargaBuah, objek_yangMauDitambah],
	    		inputName: ""
	    	});

	    } else {

	    	//this.state.dataHargaBuah[index].name = this.state.inputName;
	    	//this.state.inputName = "";
	    	//this.state.indexBuah = -1;

	    	var new_dataHargaBuah = this.state.dataHargaBuah;
	    	new_dataHargaBuah[index].name = this.state.inputName;

	    	this.setState({
	    		dataHargaBuah: new_dataHargaBuah,
	    		inputName: "",
	    		indexBuah: -1
	    	})
	    }
  	}

  	deleteForm = (event) => {
  		var index = event.target.value;

		var new_dataHargaBuah = this.state.dataHargaBuah;
		new_dataHargaBuah.splice(index, 1);

		this.setState({
	    	dataHargaBuah: new_dataHargaBuah,
	    	inputName: "",
	    	indexBuah: -1
	    })
  	}


	render () {
	  	return (
			<>

				<h1>Tabel Harga Buah</h1>

				<table style={{border: "1px solid #000", padding: "2px"}}>
					<tr style={{backgroundColor: "grey"}}>
						<th style={{border: "1px solid #000"}}>Nama</th>
						<th style={{border: "1px solid #000"}}>Harga</th>
						<th style={{border: "1px solid #000"}}>Berat</th>
					</tr>


					{
						this.state.dataHargaBuah.map( (value, index) => {

							return (

								<tr style={{backgroundColor: "orange"}}>
									<td style={{border: "1px solid #000"}}>{value.name} </td>
						            <td style={{border: "1px solid #000"}}>{value.price} </td>
						            <td style={{border: "1px solid #000"}}>{value.weight} kg</td>
						            <td style={{border: "1px solid #000"}}>
							            <button value={index} onClick={this.editForm}>edit</button>
							            <button value={index} onClick={this.deleteForm}>delete</button>
						            </td>
					  			</tr>
					        )

				  		} )

				  	}

				</table>

				<form onSubmit={this.handleSubmit}>
		        	<label>
		            	Masukkan nama peserta:
		        	</label>          
		        	<input type="text" value={this.state.inputName} onChange={this.ubahInput}/>
		        	<button>submit</button>
		        </form>

			</>
	    )
	}
}

export default Daftar;