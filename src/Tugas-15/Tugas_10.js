import React from 'react';

class Welcome extends React.Component {
	render () {
		return <p>{this.props.name}</p>;
	}
}


class ShowPrice extends React.Component {
	render () {
		return <p>{this.props.price}</p>;
	}
}

class ShowWeight extends React.Component {
	render () {
		return <p>{this.props.weight}</p>;
	}
}


let dataHargaBuah = [
  {name: "Semangka", price: 10000, weight: 1000},
  {name: "Anggur", price: 40000, weight: 500},
  {name: "Strawberry", price: 30000, weight: 400},
  {name: "Jeruk", price: 30000, weight: 1000},
  {name: "Mangga", price: 30000, weight: 500}
]


class HargaBuah extends React.Component {
	render () {
	  	return (
			<>

				<h1>Tabel Harga Buah</h1>

				<table style={{border: "1px solid #000", padding: "2px"}}>
					<tr style={{backgroundColor: "grey"}}>
						<th style={{border: "1px solid #000"}}>Nama</th>
						<th style={{border: "1px solid #000"}}>Harga</th>
						<th style={{border: "1px solid #000"}}>Berat</th>
					</tr>


					{ dataHargaBuah.map(el => {
						return (
							<tr style={{backgroundColor: "orange"}}>
								<td style={{border: "1px solid #000"}}><Welcome name = {el.name}/></td>
					            <td style={{border: "1px solid #000"}}><ShowPrice price = {el.price}/></td>
					            <td style={{border: "1px solid #000"}}><ShowWeight weight = {(el.weight)/1000}/>kg</td>
				  			</tr>
				        )
				  	})}


				</table>
		    </>
	    )
	}
}

export default HargaBuah;