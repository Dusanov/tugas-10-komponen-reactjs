import React from "react";
import {Link} from "react-router-dom";

const Nav = () => {
  return (
    <>
      <ul>
        <li>
          <Link to="/">Tugas_9</Link>
        </li>
        <li>
          <Link to="/tugas_10">Tugas_10</Link>
        </li>
        <li>
          <Link to="/tugas_11">Tugas_11</Link>
        </li>
        <li>
          <Link to="/tugas_12">Tugas_12</Link>
        </li>
        <li>
          <Link to="/tugas_13">Tugas_13</Link>
        </li>
        <li>
          <Link to="/tugas-14">Tugas_14</Link>
        </li>
      </ul>
    </>
  )
}

export default Nav