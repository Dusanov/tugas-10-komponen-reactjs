import React from "react";
import Tugas_9 from "./Tugas_9";
import Tugas_10 from "./Tugas_10";
import Tugas_11 from "./Tugas_11";
import Tugas_12 from "./Tugas_12";
import Tugas_13 from "./Tugas_13";

import {FruitProvider} from "../Tugas-14/ContextBuah"
import ContextList from "../Tugas-14/ContextList"
import ContextForm from "../Tugas-14/ContextForm"

import { Switch, Route } from "react-router";

const Routes = () => {

  return (
    <Switch>
      <Route exact path="/">
        <Tugas_9 />
      </Route>
      <Route path="/tugas_10">
        <Tugas_10 />
      </Route>
      <Route exact path="/tugas_11">
        <Tugas_11 />
      </Route>
      <Route exact path="/tugas_12">
        <Tugas_12 />
      </Route>
      <Route exact path="/tugas_13">
        <Tugas_13 />
      </Route>
      <Route exact path="/tugas-14">
        <FruitProvider>
          <ContextList/>
          <ContextForm/>
        </FruitProvider>
      </Route>
    </Switch>
  );
};

export default Routes;