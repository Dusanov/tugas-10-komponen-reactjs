import React, {Component} from 'react'

class Timer extends Component{

  constructor(props){
    super(props)
    this.state = {
      time: 100,
      date: new Date()
    }
    this.showTime = true;
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentDidUpdate(){
    if(this.state.time == 0){
      this.showTime = false
      this.componentWillUnmount()
    }
  }

  componentWillUnmount(){
    //clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
      date: new Date()
    });
  }

  render(){

    if(this.showTime){

      return(
        <>
          <h1 style={{textAlign: "center"}}>
            {this.state.time}
          </h1>
          <h1 style={{textAlign: "center"}}>
            Sekarang jam: {this.state.date.toLocaleTimeString()}
            {this.showTime}
          </h1>
        </>
      )

    } else {
      return(null);
    }

  }
}

export default Timer;