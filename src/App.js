/*import React from 'react';
import './App.css';
//import UserInfo from './Tugas-9/UserInfo';
import BuahHooks from './Tugas-13/BelajarHooks2';
//import Timer from './Tugas-11/coba';

function App() {
  return (
    <div>
      <BuahHooks />
    </div>
  );
}

export default App;*/

/*import React from "react"
import {FruitProvider} from "./Tugas-14/ContextBuah"
import ContextList from "./Tugas-14/ContextList"
import ContextForm from "./Tugas-14/ContextForm"

const Fruit = () =>{
  return(
    <FruitProvider>
      <ContextList/>
      <ContextForm/>
    </FruitProvider>
  )
}

export default Fruit*/

import React from 'react';
import Routes from "./Tugas-15/Routes";
import Nav from "./Tugas-15/NavRoutes";

import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <Router>
      <Nav />
      <Routes/>
    </Router>   
  );
}
  
export default App;